-- +migrate Up
CREATE SCHEMA IF NOT EXISTS order_service;

CREATE TYPE order_status AS ENUM ('Создан', 'Оплачен');

CREATE TABLE order_service.order
(
    id         BIGSERIAL
        CONSTRAINT pk_order_id PRIMARY KEY,
    status     order_status NOT NULL,
    created_at TIMESTAMPTZ  NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMPTZ           DEFAULT NULL
);
COMMENT ON TABLE order_service.order IS 'Заказы';

CREATE TABLE order_service.order_item
(
    id         BIGSERIAL
        CONSTRAINT pk_order_item_id PRIMARY KEY,
    order_id   BIGINT      NOT NULL,
    item_id    BIGINT      NOT NULL,
    created_at TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP
);
COMMENT ON TABLE order_service.order_item IS 'Позиции в заказе';

----------

CREATE SCHEMA IF NOT EXISTS warehouse_service;

CREATE TABLE warehouse_service.item
(
    id            BIGSERIAL
        CONSTRAINT pk_item_id PRIMARY KEY,
    name          VARCHAR     NOT NULL,
    total_amount  BIGINT      NOT NULL,
    booked_amount BIGINT      NOT NULL,
    created_at    TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at    TIMESTAMPTZ          DEFAULT NULL
);
COMMENT ON TABLE warehouse_service.item IS 'Товары';
-- +migrate Down
