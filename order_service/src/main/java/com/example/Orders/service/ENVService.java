package com.example.Orders.service;

public final class ENVService {

        // Nested
        enum EnvKeys {
            ORDER_SERVICE_POSTGRES_HOST,
            ORDER_SERVICE_POSTGRES_PORT,
            ORDER_SERVICE_POSTGRES_DB,
            ORDER_SERVICE_POSTGRES_USER,
            ORDER_SERVICE_POSTGRES_PASSWORD
        }

        // Public Methods
        String getDbHost() {
            return System.getenv(EnvKeys.ORDER_SERVICE_POSTGRES_HOST.name());
        }
        String getDbPort() {
            return System.getenv(EnvKeys.ORDER_SERVICE_POSTGRES_PORT.name());
        }
        String getDbName() {
            return System.getenv(EnvKeys.ORDER_SERVICE_POSTGRES_DB.name());
        }
        String getDbUser() {
            return System.getenv(EnvKeys.ORDER_SERVICE_POSTGRES_USER.name());
        }
        String getDbPassword() {
            return System.getenv(EnvKeys.ORDER_SERVICE_POSTGRES_PASSWORD.name());
        }
    }

