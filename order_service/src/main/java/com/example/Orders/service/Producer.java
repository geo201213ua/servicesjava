package com.example.Orders.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Service
public class Producer {
    public static String reserveItems = "reservedItems";
    public static String remoteItems = "removeItems";

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    public void publishReserveItemsMessage(String message) {
        this.kafkaTemplate.send(reserveItems, message);
    }

    public void publishRemoveItemsMessage(String message) {
        this.kafkaTemplate.send(remoteItems, message);
    }
}
