package com.example.Orders.models.orderItem;

import java.sql.Timestamp;

public enum OrderItemKeys {
    id, order_id, item_id, created_at
}
