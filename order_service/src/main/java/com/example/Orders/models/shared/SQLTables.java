package com.example.Orders.models.shared;

public enum SQLTables {
    order {
        public String toString() {
            return "order_service.order";
        }
    },
    orderItem {
        public String toString() {
            return "order_service.order_item";
        }
    }
}
