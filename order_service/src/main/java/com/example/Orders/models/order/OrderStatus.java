package com.example.Orders.models.order;

enum OrderStatus {
    created {
        public String toString() {
            return "Создан";
        }
    },
    paid {
        public String toString() {
            return "Оплачен";
        }
    }
}
