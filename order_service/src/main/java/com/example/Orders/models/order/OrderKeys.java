package com.example.Orders.models.order;

import java.sql.Timestamp;

public enum OrderKeys {
    id, status, created_at, updated_at, orderItems
}
