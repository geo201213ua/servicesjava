package com.example.Orders.models.orderItem;

import com.example.Orders.models.order.Order;
import com.example.Orders.models.order.OrderKeys;
import com.example.Orders.models.shared.SQLTables;
import com.example.Orders.service.DatabaseService;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.MessageFormat;
import java.util.ArrayList;

public class OrderItem {

    // Private Properties
    private long id;
    private long orderId;
    private long itemId;
    private Timestamp createdAt;

    private String tableName = SQLTables.orderItem.toString();

    // Empty Constructor
    public OrderItem() {}

    // Private Constructor
    private OrderItem(long id, long orderId, long itemId, Timestamp createdAt) {
        this.id = id;
        this.orderId = orderId;
        this.itemId = itemId;
        this.createdAt = createdAt;
    }

    // Public Methods

    public JSONArray getJsonItems(long id) throws JSONException, SQLException {
        return getJsonFrom(this.getItems(id));
    }

    public String getJsonToPut(JSONArray jsonArray, long orderId) throws JSONException {
        var insertCommand = "INSERT INTO" +
                " " +
                tableName +
                "(id, order_id, item_id, created_at)";

        var values = " VALUES ";
        for(int i=0; i<jsonArray.length(); i++){
            JSONObject jsonObj = jsonArray.getJSONObject(i);
            var value = "({0}, {1}, {2}, {3})";
            var valueWithInsertedValues = MessageFormat.format(
                    value,
                    jsonObj.get(OrderItemKeys.id.name()),
                    orderId,
                    jsonObj.get(OrderItemKeys.item_id.name()),
                    "'" + jsonObj.get(OrderItemKeys.created_at.name()) + "'"
            );

            if (i == jsonArray.length() - 1) {
                values += valueWithInsertedValues + ";";
            } else {
                values += valueWithInsertedValues + ",";
            }
        }
        return insertCommand + values;
    }

    // Private Methods

    private ArrayList<OrderItem> getItems(long id) throws SQLException {
        var connection = new DatabaseService().getDBConnection();
        var statement = connection.createStatement();

        var sqlTask = "select * from" + " " + tableName + " WHERE order_id = " + id;
        var result = statement.executeQuery(sqlTask);
        var items = new ArrayList<OrderItem>();
        while (result.next()) {
            var item = getFromResultSet(result);
            items.add(item);
        }
        return items;
    }

    private JSONObject getJsonFrom(OrderItem orderItem) throws JSONException {
        var json = new JSONObject();
        json.put(OrderItemKeys.id.name(), orderItem.id);
        json.put(OrderItemKeys.item_id.name(), orderItem.itemId);
        json.put(OrderItemKeys.order_id.name(), orderItem.orderId);
        json.put(OrderKeys.created_at.name(), orderItem.createdAt.toString());
        return json;
    }

    private JSONArray getJsonFrom(ArrayList<OrderItem> items) throws JSONException {
        JSONArray jsonArray = new JSONArray();
        for (OrderItem item : items) {
            jsonArray.put(this.getJsonFrom(item));
        }
        return  jsonArray;
    }

    private OrderItem getFromResultSet(ResultSet resultSet) throws SQLException {
        long id = resultSet.getInt(OrderItemKeys.id.name());
        long orderId = resultSet.getLong(OrderItemKeys.order_id.name());
        long itemId = resultSet.getLong(OrderItemKeys.item_id.name());
        Timestamp createdAt = resultSet.getTimestamp(OrderKeys.created_at.name());
        return new OrderItem(id, orderId, itemId, createdAt);
    }


}
