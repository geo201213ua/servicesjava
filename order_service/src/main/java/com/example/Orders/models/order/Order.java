package com.example.Orders.models.order;

import com.example.Orders.models.orderItem.OrderItem;
import com.example.Orders.models.shared.SQLTables;
import com.example.Orders.service.DatabaseService;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.MessageFormat;
import java.util.ArrayList;

public class Order {

    // Private Properteies

    private long id;
    private String status;
    private Timestamp createdAt;
    private Timestamp updatedAt;
    private String tableName = SQLTables.order.toString();
    private OrderItem orderItem = new OrderItem();

    // Empty Constructor

    public Order() {}

    // Private Constructor
    private Order(long id, String status, Timestamp created_at, Timestamp updated_at) {
        this.id = id;
        this.status = status;
        this.createdAt = created_at;
        this.updatedAt = updated_at;
    }

    // Public Methods
    public void putJson(JSONObject json) throws SQLException, JSONException {
        var connection = new DatabaseService().getDBConnection();
        var statement = connection.createStatement();
        var rawString = "INSERT INTO" +
                " " +
                tableName +
                "(id, status, created_at, updated_at)" +
                " " +
                "VALUES ({0}, {1}, {2}, {3})";
        try {
            var sql = MessageFormat.format(
                    rawString,
                    json.get(OrderKeys.id.name()),
                    "'" + stateFor(json.getInt(OrderKeys.status.toString())) + "'",
                    "'" + json.get(OrderKeys.created_at.name()) + "'",
                    "'" + json.get(OrderKeys.updated_at.name()) + "'"
            );

            var orderItems = getOrderItemsString(json);
            sql += "; " +  orderItems;
            statement.executeQuery(sql);
        } catch (Exception e) {
            throw e;
        }
    }

    // Public Methods
    public JSONObject getJsonItems(long id) throws JSONException, SQLException {
        var orderItems = getOrderItemWithOrderId(id);
        var order = getJsonFrom(this.getItems(id));
        var json = new JSONObject();
        json.put("orderItems", orderItems);
        json.put("orders", order);
        return json;
    }

    // Private Methods
    private ArrayList<Order> getItems(long id) throws SQLException {
        var connection = new DatabaseService().getDBConnection();
        var statement = connection.createStatement();

        var sqlTask = "select * from" + " " + tableName + " WHERE id = " + id;
        var result = statement.executeQuery(sqlTask);
        var items = new ArrayList<Order>();
        while (result.next()) {
            var item = getFromResultSet(result);
            items.add(item);
        }
        return items;
    }

    private String getOrderItemsString(JSONObject jsonObject) throws JSONException {
        var jsonArrOrderItem = jsonObject.getJSONArray(OrderKeys.orderItems.name());
        return orderItem.getJsonToPut(jsonArrOrderItem, jsonObject.getLong(OrderKeys.id.name()));
    }

    private JSONArray getOrderItemWithOrderId(long id) throws SQLException, JSONException {
        return orderItem.getJsonItems(id);
    }

    private Order getFromResultSet(ResultSet resultSet) throws SQLException {
        long id = resultSet.getInt(OrderKeys.id.name());
        String status = resultSet.getString(OrderKeys.status.name());
        Timestamp createdAt = resultSet.getTimestamp(OrderKeys.created_at.name());
        Timestamp updatedAt = resultSet.getTimestamp(OrderKeys.updated_at.name());
        return new Order(id, status, createdAt, updatedAt);
    }

    private JSONObject getJsonFrom(Order order) throws JSONException {
        var json = new JSONObject();
        json.put(OrderKeys.id.name(), order.id);
        json.put(OrderKeys.status.name(), order.status);
        json.put(OrderKeys.created_at.name(), order.createdAt.toString());
        json.put(OrderKeys.updated_at.name(), order.updatedAt.toString());
        return json;
    }

    private JSONArray getJsonFrom(ArrayList<Order> items) throws JSONException {
        JSONArray jsonArray = new JSONArray();
        for (Order item : items) {
            jsonArray.put(this.getJsonFrom(item));
        }
        return  jsonArray;
    }

    private String stateFor(int index) {
        if (index == 0) {
            return OrderStatus.created.toString();
        } else {
            return OrderStatus.paid.toString();
        }
    }
}
