package com.example.Orders.Controller;

import com.example.Orders.models.order.Order;
import com.example.Orders.service.Producer;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class OrderController {

    @Autowired
    static Producer producer;

    @PostMapping(path="/create", consumes = "application/json", produces = "application/json")
    public String create(@RequestBody String model) {
        try {
            JSONObject json = new JSONObject(model);
            Order order = new Order();
            order.putJson(json);
            producer.publishReserveItemsMessage("TEST");
            return "Successfully added";
        } catch (Exception e) {
            return getErrorMessage(e);
        }
    }

    @PostMapping(path="/get", consumes = "application/json", produces = "application/json")
    public String get(@RequestBody String model) {
        try {
            JSONObject json = new JSONObject(model);
            var id = getIdFrom(json);
            Order order = new Order();
            return order.getJsonItems(id).toString();
        } catch (Exception e) {
            return getErrorMessage(e);
        }
    }

    @PostMapping(path="/payout", consumes = "application/json", produces = "application/json")
    public String payout(@RequestBody String model) {
        System.out.println(model);
        return "asdf";
    }

    // Private Methods

    private long getIdFrom(JSONObject json) throws JSONException {
        return json.getLong("id");
    }

    private String getErrorMessage(Exception e) {
        return "Error - " + e.getMessage() + "\n" + "Exception - " + e;
    }
}
