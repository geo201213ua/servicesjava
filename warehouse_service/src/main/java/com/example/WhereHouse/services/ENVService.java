package com.example.WhereHouse.services;

public final class ENVService {

    // Nested
    enum EnvKeys {
        WAREHOUSE_SERVICE_POSTGRES_HOST,
        WAREHOUSE_SERVICE_POSTGRES_PORT,
        WAREHOUSE_SERVICE_POSTGRES_DB,
        WAREHOUSE_SERVICE_POSTGRES_USER,
        WAREHOUSE_SERVICE_POSTGRES_PASSWORD
    }

    // Public Methods
    String getDbHost() {
        return System.getenv(EnvKeys.WAREHOUSE_SERVICE_POSTGRES_HOST.name());
    }
    String getDbPort() {
        return System.getenv(EnvKeys.WAREHOUSE_SERVICE_POSTGRES_PORT.name());
    }
    String getDbName() {
        return System.getenv(EnvKeys.WAREHOUSE_SERVICE_POSTGRES_DB.name());
    }
    String getDbUser() {
        return System.getenv(EnvKeys.WAREHOUSE_SERVICE_POSTGRES_USER.name());
    }
    String getDbPassword() {
        return System.getenv(EnvKeys.WAREHOUSE_SERVICE_POSTGRES_PASSWORD.name());
    }
}
