package com.example.WhereHouse.services;
import java.sql.*;

public final class DatabaseService {

    // Private Properties
    private ENVService envService;

    // Constructor
    public DatabaseService() {
        this.envService = new ENVService();
    }

    // Public methods
    public Connection getDBConnection() throws SQLException {
        var userName = envService.getDbUser();
        var password = envService.getDbPassword();
        var dbName = envService.getDbName();
        var host = envService.getDbHost();
        var port = envService.getDbPort();
        String url = "jdbc:postgresql://" + host + ":" + port + "/" + dbName;
        final Connection connection = DriverManager.getConnection(url, userName, password);
        return connection;
    }
}
