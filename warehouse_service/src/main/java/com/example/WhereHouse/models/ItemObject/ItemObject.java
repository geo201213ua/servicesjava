package com.example.WhereHouse.models.ItemObject;

import com.example.WhereHouse.models.SQLTables;
import com.example.WhereHouse.services.DatabaseService;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.sql.*;
import java.text.MessageFormat;
import java.util.ArrayList;

public final class ItemObject implements ItemObjectInterface {

    // Private Properties
    private long id;
    private String name;
    private long totalAmount;
    private long bookedAmount;
    private Timestamp createdAt;
    private Timestamp updatedAt;
    private final String tableName = SQLTables.item.toString();

    // Public empty Construct
    public ItemObject() {}

    // Private Constructor
    private ItemObject(
            long id,
            String name,
            long totalAmount,
            long bookedAmount,
            Timestamp createdAt,
            Timestamp updatedAt
    ) {
        this.id = id;
        this.name = name;
        this.totalAmount = totalAmount;
        this.bookedAmount = bookedAmount;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    // Public Properteis
    public JSONArray getJsonItems() throws JSONException, SQLException {
        return getJsonFrom(this.getItems());
    }

    public void putJson(JSONObject json) throws SQLException, JSONException {
        var connection = new DatabaseService().getDBConnection();
        var statement = connection.createStatement();
        var rawString = "INSERT INTO" +
                " " +
                tableName +
                "(id, name, total_amount, booked_amount, created_at, updated_at)" +
                " " +
                "VALUES ({0}, {1}, {2}, {3}, {4}, {5})";
        try {
            var sql = MessageFormat.format(
                    rawString,
                    json.get(ItemKeysEnum.id.name()),
                    "'" + json.get(ItemKeysEnum.name.name()) + "'",
                    json.get(ItemKeysEnum.total_amount.name()),
                    json.get(ItemKeysEnum.booked_amount.name()),
                    "'" + json.get(ItemKeysEnum.created_at.name()) + "'",
                    "'" + json.get(ItemKeysEnum.updated_at.name()) + "'"
            );
            statement.executeQuery(sql);
        } catch (Exception e) {
            throw e;
        }
    }

    // Private Methods
    private ItemObject getFromResultSet(ResultSet resultSet) throws SQLException {
        long id = resultSet.getInt(ItemKeysEnum.id.name());
        String name = resultSet.getString(ItemKeysEnum.name.name());
        long totalAmount = resultSet.getInt(ItemKeysEnum.total_amount.name());
        long bookedAmount = resultSet.getInt(ItemKeysEnum.booked_amount.name());
        Timestamp createdAt = resultSet.getTimestamp(ItemKeysEnum.created_at.name());
        Timestamp updatedAt = resultSet.getTimestamp(ItemKeysEnum.updated_at.name());
        return new ItemObject(id, name, totalAmount, bookedAmount, createdAt, updatedAt);
    }

    private ArrayList<ItemObject> getItems() throws SQLException {
        var connection = new DatabaseService().getDBConnection();
        var statement = connection.createStatement();
        var sqlTask = "select * from" + " " + tableName;
        var result = statement.executeQuery(sqlTask);
        var items = new ArrayList<ItemObject>();
        while (result.next()) {
            var item = getFromResultSet(result);
            items.add(item);
        }
        return items;
    }

    private JSONObject getJsonFrom(ItemObject item) throws JSONException {
        var json = new JSONObject();
        json.put(ItemKeysEnum.id.name(), item.id);
        json.put(ItemKeysEnum.name.name(), item.name);
        json.put(ItemKeysEnum.total_amount.name(), item.totalAmount);
        json.put(ItemKeysEnum.booked_amount.name(), item.bookedAmount);
        json.put(ItemKeysEnum.created_at.name(), item.createdAt.toString());
        json.put(ItemKeysEnum.updated_at.name(), item.updatedAt.toString());
        return json;
    }

    private JSONArray getJsonFrom(ArrayList<ItemObject> items) throws JSONException {
        JSONArray jsonArray = new JSONArray();
        for (ItemObject item : items) {
            jsonArray.put(this.getJsonFrom(item));
        }
        return  jsonArray;
    }
}
