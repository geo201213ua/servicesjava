package com.example.WhereHouse.models.ItemObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.SQLException;

public interface ItemObjectInterface {
    JSONArray getJsonItems() throws JSONException, SQLException;
    void putJson(JSONObject json) throws SQLException, JSONException;
}
