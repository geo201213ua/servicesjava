package com.example.WhereHouse.controllers;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class KafkaConsumer {

    @KafkaListener(topics="reservedItems",groupId="shitGroup")
    public void consumeFromReserve(String message) {
        System.out.println("---------\n" + message + "---------\n");
    }

    @KafkaListener(topics="removeItems",groupId="shitGroup")
    public void consumeFromRemoveItems(String message) {
        System.out.println(message);
    }

}
