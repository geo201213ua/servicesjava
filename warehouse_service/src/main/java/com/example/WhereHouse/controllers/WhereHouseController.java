package com.example.WhereHouse.controllers;

import com.example.WhereHouse.models.ItemObject.ItemObject;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.*;

@RestController
public class WhereHouseController {

    // API Methods
    @PostMapping(path="/listItems", consumes = "application/json", produces = "application/json")
    public String listItems(@RequestBody String model) {
        System.out.println(model);
        ItemObject itemObject = new ItemObject();
        try {
            JSONArray jsonArray = itemObject.getJsonItems();
            return jsonArray.toString();
        } catch (Exception e) {
           return this.getErrorMessage(e);
        }
    }

    @PostMapping(path="/addItem", consumes = "application/json", produces = "application/json")
    public String addItem(@RequestBody String model) {
        try {
            JSONObject json = new JSONObject(model);
            ItemObject itemObject = new ItemObject();
            itemObject.putJson(json);
            return this.getSuccessMessage(json);
        } catch (Exception e) {
            return this.getErrorMessage(e);
        }
    }

    // Private Methods

    private String getSuccessMessage(JSONObject jsonObject) {
        return jsonObject.toString() + "\n Added successfully";
    }

    private String getErrorMessage(Exception e) {
        return "Error - " + e.getMessage() + "\n" + "Exception - " + e;
    }
}
